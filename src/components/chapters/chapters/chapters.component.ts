import {Component, OnDestroy, OnInit} from '@angular/core';
import {LordLibService} from 'lord-lib';
import {ChapterModel, IdentityChapterModel} from 'lord-lib/lib/data';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-chapters',
  templateUrl: './chapters.component.html',
  styleUrls: ['./chapters.component.sass']
})
export class ChaptersComponent implements OnInit, OnDestroy {

  constructor(private lordLibService: LordLibService) {
  }

  public chapters: IdentityChapterModel[];

  public ngOnInit(): void {
    this.lordLibService.getChapters().pipe(takeUntil(this.alive$)).subscribe((chapters: ChapterModel) => {
      this.chapters = [...chapters.docs].slice(0, 8);
    });
  }

  public ngOnDestroy() {
    this.alive$.next();
    this.alive$.complete();
  }

  private alive$ = new Subject<void>();
}

