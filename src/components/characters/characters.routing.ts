import {RouterModule, Routes} from '@angular/router';
import {CharactersComponent} from './characters/characters.component';

const routes: Routes = [
  {
    path: '',
    component: CharactersComponent,
    data: {
      title: 'Characters',
      headerDisplay: 'none'
    }
  }
];

export const CharactersRouting = RouterModule.forChild(routes);


