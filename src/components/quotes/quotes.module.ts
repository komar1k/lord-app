import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { QuotesComponent } from './quotes/quotes.component';
import {QuotesRouting} from './quotes.routing';


@NgModule({
  imports: [
    CommonModule,
    QuotesRouting
  ],
  declarations: [QuotesComponent]
})
export class QuotesModule {
}
