import {RouterModule, Routes} from '@angular/router';
import {MoviesComponent} from './movies/movies.component';

const routes: Routes = [
  {
    path: '',
    component: MoviesComponent,
    data: {
      title: 'Movies',
      headerDisplay: 'none'
    }
  }
];

export const MoviesRouting = RouterModule.forChild(routes);


