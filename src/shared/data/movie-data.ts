export interface MoviesModel {
  docs: IdentityMovieModel[];
  total: number;
  limit: number;
  offset: number;
  page: number;
  pages: number;
}

export interface IdentityMovieModel {
  _id: string;
  name: string;
  runtimeInMinutes: number;
  budgetInMillions: number;
  boxOfficeRevenueInMillions: number;
  academyAwardNominations: number;
  academyAwardWins: number;
  rottenTomatesScore: number;
}
