import {NgModule} from '@angular/core';
import { BooksComponent } from './books/books.component';
import {CommonModule} from '@angular/common';
import {BooksRouting} from './book.routing';


@NgModule({
  imports: [
    CommonModule,
    BooksRouting
  ],
  declarations: [BooksComponent]
})
export class BooksModule {
}
