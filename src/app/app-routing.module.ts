import {RouterModule, Routes} from '@angular/router';
import {WrapperComponent} from '../shared/layouts/wrapper';

const routes: Routes = [
  {
    path: '',
    component: WrapperComponent,
    children: [
      {
        path: 'books',
        loadChildren: () => import('../components/books/books.module').then(m => m.BooksModule),
      },
      {
        path: 'movies',
        loadChildren: () => import('../components/movies/movies.module').then(m => m.MoviesModule),
      },
      {
        path: 'characters',
        loadChildren: () => import('../components/characters/characters.module').then(m => m.CharactersModule),
      },
      {
        path: 'quotes',
        loadChildren: () => import('../components/quotes/quotes.module').then(m => m.QuotesModule),
      },
      {
        path: 'chapters',
        loadChildren: () => import('../components/chapters').then(m => m.ChaptersModule),
      },
    ]
  },
];

export const AppRoutes = RouterModule.forRoot(routes, {initialNavigation: 'enabled'});
