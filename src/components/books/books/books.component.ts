import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {LordLibService} from 'lord-lib';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.sass']
})
export class BooksComponent implements OnInit, OnDestroy {

  constructor(private theLordOfTheRingsLibService: LordLibService) { }

  public booksList: string[];

  public ngOnInit(): void {
    this.theLordOfTheRingsLibService.getBooks().pipe(takeUntil(this.alive$)).subscribe(books => {
      this.booksList = books.docs.map(book => book.name);
    });
  }

  public ngOnDestroy() {
    this.alive$.next();
    this.alive$.complete();
  }

  private alive$ = new Subject<void>();
}
