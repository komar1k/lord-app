import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { MoviesComponent } from './movies/movies.component';
import {MoviesRouting} from './movies.routing';


@NgModule({
  imports: [
    CommonModule,
    MoviesRouting
  ],
  declarations: [MoviesComponent]
})
export class MoviesModule {
}
