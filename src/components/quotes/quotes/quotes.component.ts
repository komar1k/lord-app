import {Component, OnDestroy, OnInit} from '@angular/core';
import {LordLibService} from 'lord-lib';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {IdentityQuoteModel, QuoteModel} from 'lord-lib/lib/data';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.sass']
})
export class QuotesComponent implements OnInit, OnDestroy {

  constructor(private lordLibService: LordLibService) {
  }

  public quotesList: IdentityQuoteModel[];

  public ngOnInit(): void {
    this.lordLibService.getQuotes().pipe(takeUntil(this.alive$)).subscribe((quotes: QuoteModel) => {
      this.quotesList = [...quotes.docs].slice(0, 8);
    });
  }

  public ngOnDestroy() {
    this.alive$.next();
    this.alive$.complete();
  }

  private alive$ = new Subject<void>();
}
