import {RouterModule, Routes} from '@angular/router';
import {QuotesComponent} from './quotes/quotes.component';

const routes: Routes = [
  {
    path: '',
    component: QuotesComponent,
    data: {
      title: 'Quotes',
      headerDisplay: 'none'
    }
  }
];

export const QuotesRouting = RouterModule.forChild(routes);


