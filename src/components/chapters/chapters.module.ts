import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChaptersComponent} from './chapters/chapters.component';
import {ChaptersRouting} from './chapters.routing';


@NgModule({
  imports: [
    CommonModule,
    ChaptersRouting
  ],
  declarations: [ChaptersComponent]
})
export class ChaptersModule {
}
