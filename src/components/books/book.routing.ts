import {RouterModule, Routes} from '@angular/router';
import {BooksComponent} from './books/books.component';

const routes: Routes = [
  {
    path: '',
    component: BooksComponent,
    data: {
      title: 'Books',
      headerDisplay: 'none'
    }
  }
];

export const BooksRouting = RouterModule.forChild(routes);


