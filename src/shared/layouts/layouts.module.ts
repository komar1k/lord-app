import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {HeaderComponent} from './header';
import {WrapperComponent} from './wrapper';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    HeaderComponent,
    WrapperComponent
  ],
  exports: [WrapperComponent, HeaderComponent],
})
export class LayoutsModule {
}
