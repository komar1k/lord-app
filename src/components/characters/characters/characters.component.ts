import {Component, OnDestroy, OnInit} from '@angular/core';
import {LordLibService} from 'lord-lib';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {CharacterModel, IdentityCharacterModel} from 'lord-lib/lib/data';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.sass']
})
export class CharactersComponent implements OnInit, OnDestroy {

  constructor(private lordLibService: LordLibService) {
  }

  public characters: IdentityCharacterModel[];

  public ngOnInit(): void {
    this.lordLibService.getListOfCharacters().pipe(takeUntil(this.alive$)).subscribe((characters: CharacterModel) => {
      this.characters = [...characters.docs].slice(0, 8);
    });
  }

  public ngOnDestroy() {
    this.alive$.next();
    this.alive$.complete();
  }

  private alive$ = new Subject<void>();
}
