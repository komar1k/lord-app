import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CharactersComponent} from './characters/characters.component';
import {CharactersRouting} from './characters.routing';


@NgModule({
  imports: [
    CommonModule,
    CharactersRouting
  ],
  declarations: [CharactersComponent]
})
export class CharactersModule {
}
