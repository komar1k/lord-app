import {RouterModule, Routes} from '@angular/router';
import {ChaptersComponent} from './chapters/chapters.component';

const routes: Routes = [
  {
    path: '',
    component: ChaptersComponent,
    data: {
      title: 'Chapters',
      headerDisplay: 'none'
    }
  }
];

export const ChaptersRouting = RouterModule.forChild(routes);


