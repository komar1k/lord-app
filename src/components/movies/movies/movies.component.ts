import {Component, OnInit, OnDestroy} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {IdentityMovieModel, MoviesModel} from '../../../shared/data';
import {LordLibService} from 'lord-lib';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.sass']
})
export class MoviesComponent implements OnInit, OnDestroy {

  constructor(private lordLibService: LordLibService) {}

  public movieList: IdentityMovieModel[];

  public ngOnInit(): void {
    this.lordLibService.getListOfMovies().pipe(takeUntil(this.alive$)).subscribe((movies: MoviesModel) => {
      this.movieList = movies.docs;
    });
  }

  public ngOnDestroy() {
    this.alive$.next();
    this.alive$.complete();
  }

  private alive$ = new Subject<void>();
}
